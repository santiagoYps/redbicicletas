const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if(process.env.NODE_ENV === 'production'){
  const options = {
    auth: {
      api_key: process.env.SEND_GRID_API_KEY
    }
  }
  mailConfig = sgTransport(options);
}
else{
  if (process.env.NODE_ENV === 'staging'){
    console.log('Development Enviroment');
    const options = {
      auth: {
        api_key: process.env.SEND_GRID_API_KEY
      }
    }
    mailConfig = sgTransport(options);
  }
  else{
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: process.env.ethereal_user,
        pass: process.env.ethereal_pwd
      }
    }
  }
}


let transporter = nodemailer.createTransport(mailConfig);

// verify connection configuration
transporter.verify(function(error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages");
  }
});

const sendMail = function(email, token){
  let message = {
    from: process.env.from_email,
    to: email, //kenyon.durgan@ethereal.email
    subject: "Verificación de cuenta. Red Bicicletas",
    text: `Hola.\nPor favor, para verificar tu cuenta haga click en el siguiente link:\n
       http://localhost:3000/token/confirmation/${token}
       \n\nAtt: Red bicicletas`,
    html: `<p>Hola.
      <br>Por favor, para verificar tu cuenta haga click en el siguiente link:
      <br><a href="http://localhost:3000/token/confirmation/${token}">Enlace</a>
      <br>Att: Red bicicletas.</p>`
  };

  return transporter.sendMail(message);
}

const sendPersonalizedMail = function(mailOptions){
  return transporter.sendMail(mailOptions);
}

exports.sendMail = sendMail;
exports.sendPersonalizedMail = sendPersonalizedMail;