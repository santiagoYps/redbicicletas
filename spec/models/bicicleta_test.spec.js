const mongoose = require('mongoose');
const Bicicleta = require('../../models/Bicicleta');

const mongoDB = 'mongodb://localhost/testdb';

beforeAll((done)=>{
  mongoose.connection.close().then(()=>{
    mongoose.connect(mongoDB, {useNewUrlParser: true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log('We are connected to test database');
      done();
    });
  })
  .catch(error => console.log(error));
})

describe('Testing bicicletas', function(){

  // beforeEach(function(done) {
  //   mongoose.connect(mongoDB, {useNewUrlParser: true});

  //   const db = mongoose.connection;
  //   db.on('error', console.error.bind(console, 'connection error:'));
  //   db.once('open', function() {
  //     console.log('We are connected to test database');
  //     done();
  //   });
  // });

  afterEach(function(done){
    Bicicleta.deleteMany({}, function(err, success){
      if (err)
        console.log(err);
      done();
    });
  });

  describe('Bicicleta.createInstance', ()=> {
    it('Crea una instancia de Bicicleta', function(){
      let bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe('verde');
      expect(bici.modelo).toBe('urbana');
      expect(bici.ubicacion[0]).toEqual(-34.5);
      expect(bici.ubicacion[1]).toEqual(-54.1);
    });
  });

  describe('Bicicleta.allBicis', ()=> {
    it('Comienza vacia', (done)=>{
      Bicicleta.allBicis()
      .then(bicis=>{
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe('Bicicleta.add() async/await', ()=>{
    it('Agrega solo una bici', async (done)=>{
      let bici = new Bicicleta({code:2, color:'roja', modelo:'montaña'});
      try{
        const newBici = await Bicicleta.add(bici);
        expect(newBici.code).toEqual(bici.code);
        const bicis = await Bicicleta.allBicis();
        expect(bicis.length).toBe(1);
        expect(bicis[0].code).toEqual(bici.code);
        done();
      }
      catch(error){
        console.error(error);
      }      

      //      Usando Callback Functions 
      // Bicicleta.add(bici, function(err, newBici){
      //   if(err)
      //     console.log(err);
      //   Bicicleta.allBicis(function(err, bicis){
      //     if (err)
      //       console.log(err);
      //     expect(bicis.length).toEqual(1);
      //     expect(bicis[0].code).toEqual(newBici.code);
      //     done();
      //   });
      // });
    });
  });

  describe('Bicicleta.add() Promises', ()=>{
    it('Agrega solo una bici', (done)=>{
      let bici = new Bicicleta({code:2, color:'roja', modelo:'montaña'});
      Bicicleta.add(bici).then(newBici=>{
        expect(newBici.code).toEqual(bici.code);
        return Bicicleta.allBicis();
      })
      .then( bicis => {
        expect(bicis.length).toEqual(1);
        expect(bicis[0].code).toEqual(bici.code);
        done();
      })
      .catch(error => console.log(error));
    });
  });


  describe('Bicicleta.findByCode() async/await', ()=>{
    it('Busca una bici con un code dado', async (done)=>{
      let bici = new Bicicleta({code:3, color:'negra', modelo:'montaña'});      try{
        const newBici = await Bicicleta.add(bici);
        const biciFound = await Bicicleta.findByCode(bici.code)
        expect(biciFound.code).toBe(bici.code);
        expect(biciFound.color).toBe(bici.color);
        expect(biciFound.modelo).toBe(bici.modelo);
        done();
      }catch(error){
        console.log(error);
      }
    });
  });

  describe('Bicicleta.findByCode() Promises', ()=>{
    it('Busca una bici con un code dado', (done)=>{
      let bici = new Bicicleta({code:3, color:'negra', modelo:'montaña'});
      Bicicleta.add(bici)
      .then(newBici=>{
        return Bicicleta.findByCode(bici.code);
      })
      .then(biciFound=>{
        expect(biciFound.code).toBe(bici.code);
        expect(biciFound.color).toBe(bici.color);
        expect(biciFound.modelo).toBe(bici.modelo);
        done();
      })
      .catch(error => console.log(error));
    });
  });

  // describe('Bicicleta.findByCode()', ()=>{
  //   it('Busca una bici con un code dado', (done)=>{
  //     let bici = new Bicicleta({code:3, color:'roja', modelo:'montaña'});
  //     Bicicleta.add(bici, function(err, newBici){
  //       if (err)
  //         console.log(err);
  //       Bicicleta.findByCode(newBici.code, function(err, biciFound){
  //         if (err)
  //           console.log(err);
  //         expect(biciFound.code).toBe(bici.code);
  //         done();
  //       });
  //     });
  //   });
  // });

  describe('Bicicleta.deleteOne() Async/await', ()=>{
    it('Elimina un documento', async (done)=>{
      let bici = new Bicicleta({code:4, color:'roja', modelo:'montaña'});
      try{
        const newBici = await Bicicleta.add(bici);
        let bicis = await Bicicleta.allBicis();
        expect(bicis.length).toBe(1);
        const result = await Bicicleta.remove(newBici.code);
        //console.log(result);
        bicis = await Bicicleta.allBicis();
        expect(bicis.length).toBe(0);
        done();
      }catch(error){
        console.log(error);
      }        
    });
  });

  describe('Bicicleta.deleteOne() Promises', ()=>{
    it('Elimina un documento', (done)=>{
      let bici = new Bicicleta({code:4, color:'roja', modelo:'montaña'});
      
      Bicicleta.add(bici)
      .then(newBici =>{
        return Bicicleta.allBicis();
      })
      .then(bicis => {
        expect(bicis.length).toBe(1);
        return Bicicleta.remove(bici.code);
      })
      .then(result => {
        //console.log(result);
        return Bicicleta.allBicis();
      })
      .then(bicis => {
        expect(bicis.length).toBe(0);
        done();
      })
      .catch(error => console.log(error));

    });
  });

  // describe('Bicicleta.deleteOne()', ()=>{
  //   it('Elimina un documento', (done)=>{
  //     let bici = new Bicicleta({code:4, color:'roja', modelo:'montaña'});
  //     Bicicleta.add(bici, function(err, newBici){
  //       if (err)
  //         console.log(err);
  //       Bicicleta.allBicis(async function(err, bicis){
  //         if (err)
  //           console.log(err);
  //         expect(bicis.length).toEqual(1);
          
  //         Bicicleta.remove(newBici.code)
  //         .then(()=>{
  //           Bicicleta.allBicis(function(err, bicis2){
  //             if (err)
  //               console.log(err);
  //             console.log(bicis2);
  //             expect(bicis2.length).toEqual(0);
  //             done();            
  //           });
  //         });          
  //       });
  //     });
  //   });
  // });

});




/*      TESTING PARA MODELO SIN PERSISTENCIA      */

/* 
beforeEach( ()=> { Bicicleta.allBicis = [] });

describe('Bicicleta', ()=>{
  it('AllBicis comienza vacia', ()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
  });

  it('add() agrega una bici', ()=>{
    let bici = new Bicicleta(10, 'negra', 'urbana', [0.831020, -77.648071]);
    Bicicleta.add(bici);
    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(bici);
  });

  it('findById(): Devuelve bici con id 1', ()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
    Bicicleta.add(new Bicicleta(1, 'roja', 'urbana', [0.831020, -77.648071]));
    Bicicleta.add(new Bicicleta(2, 'verde', 'montaña', [0.831020, -77.648071]));
    let bici = Bicicleta.findById(1);
    expect(bici.id).toBe(1);
    expect(bici.color).toBe('roja');
    expect(bici.modelo).toBe('urbana');
  });

  it('remove(): la lista debe mantenerse sin bicis', ()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
    Bicicleta.add(new Bicicleta(1, 'roja', 'urbana', [0.831020, -77.648071]));
    Bicicleta.remove(1);
    expect(Bicicleta.allBicis.length).toBe(0);
  });

  it('update(): La bici agregada debe tener el color y modelo editados', ()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
    Bicicleta.add(new Bicicleta(1, 'roja', 'urbana', [0.831020, -77.648071]));
    Bicicleta.update(new Bicicleta(1, 'azul', 'montaña', [0.831020, -77.648071]));
    expect(Bicicleta.findById(1).color).toBe('azul');
    expect(Bicicleta.findById(1).modelo).toBe('montaña');
  });
}); */
