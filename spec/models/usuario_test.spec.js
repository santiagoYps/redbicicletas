const mongoose = require('mongoose');
const Bicicleta = require('../../models/Bicicleta');
const Usuario = require('../../models/Usuario');
const Reserva = require('../../models/Reserva');

const mongoDB = 'mongodb://localhost/testdb';

beforeAll((done)=>{
  mongoose.connection.close().then(()=>{
    mongoose.connect(mongoDB, {useNewUrlParser: true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log('We are connected to test database');
      done();
    });
  })
  .catch(error => console.log(error));
})

describe('Testing Usuario Model', function(){

  // afterEach( function(done){
  //   Reserva.deleteMany({})
  //   .then((error, success) =>{
  //     if (error)
  //       console.log(error);
  //     console.log(success);
  //     return Usuario.deleteMany({});
  //   })
  //   .then((error, success) => {
  //     if (error)
  //       console.log(error);
  //     console.log(success);
  //     return Bicicleta.deleteMany({});
  //   })
  //   .then((error, success) => {
  //     if (error)
  //       console.log(error);
  //     console.log(success);
  //     done();
  //   })
  //   .catch(error => console.log(error));
  // });

  afterEach( async function(done){
    try{
      await Reserva.deleteMany({});
      await Usuario.deleteMany({});
      await Bicicleta.deleteMany({});
      done();    
    }
    catch(error){
      console.log(error);
      done();
    }
  });

  describe('Usuario reserva una bici', ()=>{
    it('Reserva registrada', async (done)=>{
      try{
        const usuario = new Usuario({nombre: 'Santiago'});
        console.log(await usuario.save());
        const bicicleta = new Bicicleta({code: 11, color:'negro',modelo: 'urbana'});
        console.log(await bicicleta.save());

        let hoy = new Date();
        let mañana = new Date();
        mañana.setDate(hoy.getDate()+1);
        console.log(await usuario.reservar(bicicleta.id, hoy, mañana));
        const reservas = await Reserva.find({}).populate('bicicleta').populate('usuario').exec();
        console.log(reservas);
        expect(reservas.length).toBe(1);
        expect(reservas[0].diasDeReserva()).toBe(2);
        expect(reservas[0].bicicleta.code).toBe(11);
        expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
        done();
      }catch(error){
        console.log(error);
        done();
      }
    });
  });

});
