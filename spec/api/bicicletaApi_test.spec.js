const Bicicleta = require('../../models/Bicicleta');
const mongoose = require('mongoose');
const server = require('../../bin/www');
const request = require('request');


/*      TESTING API CON MONGO DB      */

const mongoDB = 'mongodb://localhost/testdb';

beforeAll((done)=>{
  mongoose.connection.close().then(()=>{
    mongoose.connect(mongoDB, {useNewUrlParser: true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log('We are connected to test database');
      done();
    });
  })
  .catch(error => console.log(error));
})

afterAll((done)=>{
  mongoose.disconnect().then(()=>{
    done();
  })
  .catch(error => console.log(error));
});

describe('TESTING BICICLETA API CON MONGO', ()=>{  

  afterEach(done=>{
    Bicicleta.deleteMany({}, function(err, success){
      if (err)
        console.log(err);
      done();
    });
  });

  describe('GET /api/bicicletas/', ()=>{
    it('Status 200', async (done)=>{
      try{
        let bicis = await Bicicleta.allBicis();
        expect(bicis.length).toBe(0);
        let bici = new Bicicleta({
          code:10,
          color:'negra',
          modelo:'urbana',
          ubicacion:[0.831020, -77.648071]
        });
        await Bicicleta.add(bici);
        bicis = await Bicicleta.allBicis();
        expect(bicis.length).toBe(1);
        request.get('http://localhost:3000/api/bicicletas', 
        function (error, response, body) {
          if (error)
            console.log(error)
          expect(response.statusCode).toBe(200);
          console.log('GET /api/bicicletas/');
          done();
        });
      }
      catch(error){
        console.log(error);
      }      
    });
  });

  describe('POST /api/bicicletas/create', ()=>{
    it('Status 200', async (done)=>{
      try{
        let bicis = await Bicicleta.allBicis();
        expect(bicis.length).toBe(0);
      }
      catch(error){
        console.log(error);
      }     

      const headers ={'content-type': 'application/json'};
      const bici = {code:11, color:'negra', modelo:'urbana', lat:0.831020, lng:-77.648071};
      request.post(
        {
          headers: headers,
          url: 'http://localhost:3000/api/bicicletas/create',
          body: JSON.stringify(bici)
        },
        async (error, response, body)=>{
          if (error)
            console.log(error);
          // console.log(body);
          expect(response.statusCode).toBe(200);

          try{
            const bicis = await Bicicleta.allBicis();
            expect(bicis.length).toBe(1);
            const biciFound = await Bicicleta.findByCode(bici.code);
            expect(biciFound.color).toBe('negra');
            expect(biciFound.code).toBe(11);
            expect(biciFound.modelo).toBe('urbana');
            done();
          }
          catch(error){
            console.log(error);
            done();
          }
        }
      );
    });
  });


  describe('DELETE /api/bicicletas/delete', ()=>{
    it('status 204', async (done)=>{
      let bicis = await Bicicleta.allBicis();
      expect(bicis.length).toBe(0);
      let bici = new Bicicleta({code:12, color:'negra', modelo:'urbana', ubicacion:[0.831020, -77.648071] });
      await Bicicleta.add(bici);
      bicis = await Bicicleta.allBicis();
      expect(bicis.length).toBe(1);

      let headers ={'content-type': 'application/json'};
      request.delete(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/delete",
          body: JSON.stringify( {"code":12} )
        },
        async function(error, response, body){
          expect(response.statusCode).toBe(204);
          bicis = await Bicicleta.allBicis();
          expect(bicis.length).toBe(0);
          done();
        }
      );
    });
  });

  describe('PUT /api/bicicletas/update', ()=>{
    it('status 200', async (done)=>{
      try{
        let bicis = await Bicicleta.allBicis();
        expect(bicis.length).toBe(0);
        let bici = new Bicicleta({code:15, color:'none', modelo:'none', ubicacion:[0, 0] });
        await Bicicleta.add(bici);
      }
      catch(error){
        console.log(error);
      }      

      let headers ={'content-type': 'application/json'};
      let updatedBici = {"code":15, "color":"blanca", "modelo":'GTX-2', "lat":0.831021, "lng":-77.648071};
      request.put(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/update",
          body: JSON.stringify(updatedBici)
        },
        function(error, response, body){
          expect(response.statusCode).toBe(200);
          Bicicleta.findByCode(15).then(bici => {
            expect(bici.color).toEqual(updatedBici.color);
            expect(bici.modelo).toEqual(updatedBici.modelo);
            done();
          })
          .catch( error => {
            console.log(error);
            done();
          });
        }
      );
    });
  });
    
});



/*      TESTING API SIN PERSISTENCIA      */

// beforeEach(()=>{Bicicleta.allBicis=[] });

// describe('Bicicleta API', ()=>{
//   describe('GET /api/bicicletas/', ()=>{
//     it('Status 200', ()=>{
//       expect(Bicicleta.allBicis.length).toBe(0);
//       let bici = new Bicicleta(10, 'negra', 'urbana', [0.831020, -77.648071]);
//       Bicicleta.add(bici);
      
//       request.get('http://localhost:3000/api/bicicletas', 
//         (error, response, body) => {
//           expect(response.statusCode).toBe(200);
//         });
//     });
//   });

//   describe('POST /api/bicicletas/create', ()=>{
//     it('Status 200', (done)=>{
//       expect(Bicicleta.allBicis.length).toBe(0);

//       let headers ={'content-type': 'application/json'};
//       let bici = {"id":10, "color":'negra', "modelo":'urbana', "lat":0.831020, "lng":-77.648071};      
//       request.post(
//         {
//           headers: headers,
//           url: 'http://localhost:3000/api/bicicletas/create',
//           body: JSON.stringify(bici)
//         },
//         function (error, response, body){
//           expect(response.statusCode).toBe(200);
//           expect(Bicicleta.findById(10).color).toBe('negra');
//           done(); // Termina el test solo si la request ha terminado.
//         }
//       );
//     });
//   });

//   describe('DELETE /api/bicicletas/delete', ()=>{
//     it('status 204', (done)=>{
//       expect(Bicicleta.allBicis.length).toBe(0);
//       let bici = new Bicicleta(11, 'amarilla', 'GTX', [0.831020, -77.648071]);
//       Bicicleta.add(bici);

//       let headers ={'content-type': 'application/json'};
//       request.delete(
//         {
//           headers: headers,
//           url: "http://localhost:3000/api/bicicletas/delete",
//           body: JSON.stringify( {"id":12} )
//         },
//         function(error, response, body){
//           expect(response.statusCode).toBe(204);
//           done();
//         }
//       );
//     });
//   });

//   describe('PUT /api/bicicletas/update', ()=>{
//     it('status 200', (done)=>{
//       expect(Bicicleta.allBicis.length).toBe(0);
//       let bici = new Bicicleta(12, 'marron', 'GTX-1', [0.831020, -77.648071]);
//       Bicicleta.add(bici);
//       let headers ={'content-type': 'application/json'};
//       let updatedBici = {"id":12, "color":"blanca", "modelo":'GTX-2', "lat":0.831021, "lng":-77.648071}

//       request.put(
//         {
//           headers: headers,
//           url: "http://localhost:3000/api/bicicletas/update",
//           body: JSON.stringify(updatedBici)
//         },
//         function(error, response, body){
//           expect(response.statusCode).toBe(200);
//           expect(Bicicleta.findById(12).color).toEqual(updatedBici.color);
//           expect(Bicicleta.findById(12).modelo).toEqual(updatedBici.modelo);
//           done();
//         }
//       );
//     });
//   });

// });