const mongoose = require('mongoose');
const mongoDB = 'mongodb://localhost/red_bicicletas';

const connect = function(){  
  mongoose.connect(mongoDB, {useNewUrlParser: true})
  .then(()=>{
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log('We are connected to test database');
    });
  });  
}

const disconnect = async function(){
  await mongoose.disconnect();
}

exports.connect = connect;
exports.disconnect = disconnect;