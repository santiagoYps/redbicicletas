const Usuario = require('../models/Usuario');

const list = function(req, res){
  Usuario.find({}).then(usuarios => {
    res.render('usuarios/index', {usuarios});
  })
  .catch(error => {
  res.status(500).json({error: err})
});
}

const create_get = function(req, res){
  res.render('usuarios/create', {errors:{}, usuario: new Usuario()});
}

const create_post = function(req, res){
  if (req.body.password != req.body.confirm_password){
    res.render('usuarios/create', {
      errors: {
        confirm_password: {
          message: 'Las contraseñas no coinciden.'
        }
      },
      usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})
    });
    return;
  }
  else{
    Usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password})
    .then(nuevoUsuario =>{
      console.log(nuevoUsuario);
      nuevoUsuario.sendEmailBienvenida();
      res.redirect('/usuarios');
    })
    .catch(err => {
      res.render('usuarios/create',{
        errors: err.errors,
        usuario: new Usuario({nombre: req.body.nombre, email: req.body.email}) })
    });
  }
}

const update_get = function(req, res){
  Usuario.findById(req.params.id).then(usuario => {
    res.render('usuarios/update', {
      errors:{},
      usuario: usuario
    });
  })
  .catch(error => {
    console.log(error);
    res.redirect('/usuarios');
  });
}

const update_post = function(req, res){
  let updatingData = {
    nombre: req.body.nombre
  }
  Usuario.findByIdAndUpdate(req.params.id, updatingData)
  .then(usuario => {
    res.redirect('/usuarios');
    return;
  })
  .catch(error => {
    console.log(error);
    res.render('/usuarios/update', {errors: error.errors, usuario: new Usuario({nombre: req.body.name})})
  })
}

const delete_post = function (req, res){
  Usuario.findByIdAndDelete(req.params.id).then(result => {
    res.redirect('/usuarios');
  })
  .catch(error => {
    console.log(error);
    res.redirect('/usuarios');
  });
}

exports.list = list;
exports.create_get = create_get;
exports.create_post = create_post;
exports.update_get = update_get;
exports.update_post = update_post;
exports.delete_post = delete_post;