const Bicicleta = require('../models/Bicicleta');
//const mongoDB = require('../utils/mongoDB');

const bicicleta_list = function(req, res){
    //mongoDB.connect();
    Bicicleta.allBicis().then(allBicis => {
        res.render('bicicletas/index', {bicis: allBicis});
        //mongoDB.disconnect();
    })
    .catch(error => {
        console.log(error);
        //mongoDB.disconnect();
        res.render('bicicletas/index', {bicis: allBicis});
    });
}

const bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

const bicicleta_create_post = function(req, res){
    let ubicacion = [Number(req.body.lat), Number(req.body.lng)];
    let bici = new Bicicleta({
        code: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: ubicacion
    });
    // mongoDB.connect();
    Bicicleta.add(bici).then(newBici => {
        // mongoDB.disconnect();
        res.redirect('/bicicletas');
    })
    .catch(error => {
        // mongoDB.disconnect();
        console.log(error);
        res.redirect('/bicicletas/create');
    });
}

const bicicleta_delete_post = function(req, res){
    Bicicleta.removeById(req.body.id).then(result => {
        console.log('Deleting:');
        console.log(result);
        res.redirect('/bicicletas');
    })
    .catch(error => console.log(error));
}

const bicicleta_update_get = function(req, res){    
    Bicicleta.findByCode(req.params.id)
    .then(bici => {
        console.log(bici);
        //res.render('bicicletas/update', {id, color, modelo, ubicacion} = bici );
        //res.render('bicicletas/update', data = bici);  
        res.render('bicicletas/update', {bici});
    })
    .catch(error => console.log(error));
}

const bicicleta_update_post = function(req, res){
    let ubicacion = [Number(req.body.lat), Number(req.body.lng)];
    let updatingData = {
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: ubicacion
    }; 
    Bicicleta.findByCode(req.params.code)
    .then(bici => {
        return Bicicleta.updateBici(bici.id, updatingData)
    })
    .then(result => {
        console.log(result);
        res.redirect('/bicicletas');
    })
    .catch(error => {
        console.log(error);
    });
}


/*      CONTROLADOR DE BICICLETA SIN PERSISTENCIA        */

// const bicicleta_list = function(req, res){
//     res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
// }

// const bicicleta_create_get = function(req, res){
//     res.render('bicicletas/create');
// }

// const bicicleta_create_post = function(req, res){
//     let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
//     bici.ubicacion = [req.body.lat, req.body.lng];
//     Bicicleta.add(bici);

//     res.redirect('/bicicletas');
// }

// const bicicleta_delete_post = function(req, res){
//     Bicicleta.remove(req.body.id);
//     res.redirect('/bicicletas');
// }

// const bicicleta_update_get = function(req, res){    
//     let bici = Bicicleta.findById(req.params.id);
//     console.log(bici);
//     //res.render('bicicletas/update', {id, color, modelo, ubicacion} = bici );
//     //res.render('bicicletas/update', data = bici);  
//     res.render('bicicletas/update', {bici});
// }

// const bicicleta_update_post = function(req, res){
//     let data = req.body;
//     let bici = Bicicleta.findById(req.params.id);
//     bici.id = data.id;
//     bici.color = data.color;
//     bici.modelo =data.modelo;
//     bici.ubicacion = [data.lat, data.lng];

//     res.redirect('/bicicletas');
// }

exports.bicicleta_list = bicicleta_list;
exports.bicicleta_create_get = bicicleta_create_get;
exports.bicicleta_create_post = bicicleta_create_post;
exports.bicicleta_delete_post = bicicleta_delete_post;
exports.bicicleta_update_get = bicicleta_update_get;
exports.bicicleta_update_post = bicicleta_update_post;