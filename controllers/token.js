const Token = require('../models/Token');
const Usuario = require('../models/Usuario');

const confirmation_get = function(req, res){
  Token.findOne({token: req.params.token})
  .then(token => {
    if (!token){
      throw new Error('Token no encontrado o expirado');
    }
    let newData = {verificado: true}
    return Usuario.findByIdAndUpdate(token._userId, newData);
  })
  .then(usuario => {
    console.log(usuario);
    res.redirect('/');
    return;
  })
  .catch(error => {
    console.log(error);
    res.status(404).send({error: error.message});
  });
}


exports.confirmation_get = confirmation_get;