const Usuario = require('../../models/Usuario');

exports.usuarios_list = function(req, res){
  Usuario.find({}).then(usuarios => {
    res.status(200).json({usuarios: usuarios});
  })
  .catch(error => {
    res.status(500).json({error: err})
  });
}

exports.usuarios_create = function(req, res){
  let usuario = new Usuario({
    nombre: req.body.nombre
  });
  usuario.save().then(nuevoUsuario => {
    res.status(200).json({nuevoUsuario: nuevoUsuario});
  })
  .catch(error => {
    res.status(500).json({error: error});
  });
}

exports.usuarios_reservar = function(req, res){
  Usuario.findById(req.body.id).then(usuario => {
    console.log(usuario);
    return usuario.reservar(req.body.biciId, req.body.desde, req.body.hasta)
  })
  .then(reserva => {
    console.log(reserva);
    res.status(200).json({nuevaReserva: reserva});
  })
  .catch(error => {
    console.log(error);
    res.status(500).json({error: error});
  });
}

exports.usuarios_delete = function(req, res){
  Usuario.remove(req.body.id)
  .then(result => {
    res.status(200).json({resultado: result});
  })
  .catch(error => {
    res.status(409).json({error: error});
  });
}