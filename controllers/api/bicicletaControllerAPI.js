let Bicicleta = require('../../models/Bicicleta');

exports.bicicleta_list = function(req, res){
  Bicicleta.allBicis().then(bicis => {
    res.status(200).json({
      bicicletas: bicis
    });
  })
  .catch(error => {
    console.log('ERROR - List(): '+error);
    res.status(500).json({
      error: String(error)
    });
  });
}

exports.bicicleta_create = function(req, res){
  let bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo
  });
  bici.ubicacion = [req.body.lat, req.body.lng];

  Bicicleta.add(bici)
  .then(newBici => {
    res.status(200).json({
      bicicleta: newBici
    });
  })
  .catch(error => {
    console.log('ERROR - Add(): '+error);
    res.status(500).json({
      error: String(error)
    });
  });
}

exports.bicicleta_delete = function(req, res){
  Bicicleta.remove(req.body.code)
  .then(result => {
    res.status(204).send();
  });  
}

exports.bicicleta_update = function(req, res){
  Bicicleta.findByCode(req.body.code)
  .then(bici=>{
    return Bicicleta.updateBici(bici.id, {
      code: req.body.code || bici.code,
      color: req.body.color || bici.color,
      modelo: req.body.modelo || bici.modelo,
      ubicacion: [
        req.body.lat || bici.ubicacion[0], 
        req.body.lng || bici.ubicacion[1]
      ]
    });
  })
  .then(result => {
    res.status(200).json(result);
  })
  .catch(error =>{
    console.log(error);
    res.status(500).json({
      error: String(error)
    });
  }); 
}


//          API SIN PERSISTENCIA

// exports.bicicleta_list = function(req, res){
//   res.status(200).json({
//     bicicletas: Bicicleta.allBicis
//   });
// }

// exports.bicicleta_create = function(req, res){
//   let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
//   bici.ubicacion = [req.body.lat, req.body.lng];

//   Bicicleta.add(bici);
//   res.status(200).json({
//     bicicleta: bici
//   });
// }

// exports.bicicleta_delete = function(req, res){
//   Bicicleta.remove(req.body.id);
//   res.status(204).send();
// }

// exports.bicicleta_update = function(req, res){
//   let bici = Bicicleta.findById(req.body.id);
//   bici.color = req.body.color || bici.color;
//   bici.modelo = req.body.modelo || bici.color;
//   bici.ubicacion = [
//     req.body.lat || bici.ubicacion[0], 
//     req.body.lng || bici.ubicacion[1]
//   ];

//   res.status(200).json({
//     bicicletaModificada: bici
//   });
// }