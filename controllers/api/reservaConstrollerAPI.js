const Reserva = require('../../models/Reserva');

exports.reservas_list = function(req, res){
  Reserva.find({}).populate('bicicleta').populate('usuario')
  .then( reservas => {
    res.status(200).json({reservas: reservas});
  })
  .catch(error => {
    res.status(500).json({error: error});
  });
}

exports.reservas_delete = function(req, res){
  Reserva.remove(req.body.id)
  .then(result => {
    res.status(200).json({resultado: result});
  })
  .catch(error => {
    res.status(409).json({error: error});
  });
}