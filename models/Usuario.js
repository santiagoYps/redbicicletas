const mongoose = require('mongoose');
const Reserva = require('./Reserva');
const Token = require('./Token');
const Schema = mongoose.Schema;

const uniqueValidator = require('mongoose-unique-validator');
const mailer = require('../mailer/mailer');

const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;

const validateEmail = function(email){
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
}

const usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    trim: true,
    unique: true,
    required: [true, 'El email es obligatorio'],
    lowercase: true,
    validate: [validateEmail, 'Por favor, ingrese un email valido'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  password: {
    type: String,
    required: [true, 'El password es obligatorio']
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
  googleId: String,
  facebookId: String
});

// necesita tener instalado mongoose-unique-validator
usuarioSchema.plugin(uniqueValidator,
   {message: 'El {PATH} ya existe con otro usuario'});

// Antes de hacer el save, ejecuta la funciona del argumento
usuarioSchema.pre('save', function(next){
  if (this.isModified('password')){
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function(passToValidate){
  return bcrypt.compareSync(passToValidate, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta){
  let reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde: desde,
    hasta: hasta
  });
  return reserva.save();
}

usuarioSchema.methods.sendEmailBienvenida = function(){
  let token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString('hex')
  });
  
  token.save().then(nuevoToken => {
    console.log(nuevoToken);
    mailer.sendMail(this.email, nuevoToken.token)
    .then(info => {
      console.log('Email de verificación enviado: '. info);
    });
  })
  .catch(error=>{
    console.log(error);
  });
}

usuarioSchema.methods.resetPassword = function(cb){
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString('hex')
  });
  const emailDestination = this.email;
  token.save(function(err){
    if (err)
      return cb(err);
    const mailOptions = {
      from: process.env.from_email,
      to: emailDestination,
      subject: 'Restablecimiento de contraseña',
      text: `Hola.\n\nPara resetear la constraseña de su cuenta por favor
        haga click en el siguiente enlace:\n\n
        http://localhost:3000/resetPassword/${token.token}`
    }
    mailer.sendPersonalizedMail(mailOptions)
    .then(()=> {
      console.log(`Se envió un email para reestablecer password
        a ${emailDestination}.`);
    })
    .catch(error=>{
      cb(error);
    })
    cb(null);
  });
}

usuarioSchema.statics.remove = function(usuarioId){
  return this.deleteOne({_id: usuarioId});
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
  const self = this;
  console.log(condition.emails)
  self.findOne({
    $or: [
      { 'googleId': condition.id }, { 'email': condition.emails[0].value }
    ]
  }, (err, result) => {
    if (result) { // login
      callback(err, result);
    } else { // registro
      console.log('---------- CONDITION ----------');
      console.log(condition);
      let values = {};
      values.googleId = condition.id;
      values.email = condition.emails[0].value;
      values.nombre = condition.displayName || 'SIN NOMBRE';
      values.verificado = true;
      values.password = condition._json.sub;
      console.log('---------- VALUES ----------');
      console.log(values);
      self.create(values, (err, result) => {
        if (err) { console.log(err); }
        return callback(err, result);
      });
    }
  })
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
  const self = this;
  console.log(condition);
  self.findOne({
    $or: [
      { 'facebookId': condition.id }, { 'email': condition.emails[0].value }
    ]
  }, (err, result) => {
    if (result) { // login
      callback(err, result);
    } else { // registro
      console.log('---------- CONDITION ----------');
      console.log(condition);
      let values = {};
      values.facebookId = condition.id;
      values.email = condition.emails[0].value;
      values.nombre = condition.displayName || 'SIN NOMBRE';
      values.verificado = true;
      values.password = crypto.randomBytes(16).toString('hex');
      console.log('---------- VALUES ----------');
      console.log(values);
      self.create(values, (err, result) => {
        if (err) { console.log(err); }
        return callback(err, result);
      });
    }
  })
};

module.exports = mongoose.model('Usuario', usuarioSchema);