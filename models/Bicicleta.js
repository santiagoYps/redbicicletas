let mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bicicletaSchema = new Schema({
    // id no porque es reservada en Mongo
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true
        }
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

bicicletaSchema.methods.toString = function(){
    return `code: ${this.code} | color: ${this.color}`;
}

bicicletaSchema.statics.allBicis = function(){
    return this.find({}).exec();
}
// Otra forma de hacer allBicis
// bicicletaSchema.statics.allBicis = function(callback){
//     return this.find({}, callback);
// }


bicicletaSchema.statics.add = function(bici){
    return this.create(bici);
}
// bicicletaSchema.statics.add = function(bici){
//     return this.create(bici, callback);
// }


bicicletaSchema.statics.findByCode = function(biciCode){
    return this.findOne({code: biciCode}).exec();
}

bicicletaSchema.statics.remove = function(biciCode){
    return this.deleteOne({code: biciCode});
}

bicicletaSchema.statics.removeById = function(biciId){
    return this.deleteOne({_id: biciId});
}

bicicletaSchema.statics.updateBici = function(biciId, updatingData){
    return this.replaceOne({_id: biciId}, updatingData);
}

//      Modelo:
const Bicicleta = mongoose.model('Bicicleta', bicicletaSchema);

module.exports = Bicicleta;


/*      Modelo Sin persistencia     */
/*
let Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return `id: ${this.id} | color: ${color}`;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(bici){
    Bicicleta.allBicis.push(bici);
}

Bicicleta.findById = function(biciId){
    let bici = Bicicleta.allBicis.find( bici => bici.id == biciId);
    if (bici)
        return bici;
    else
        throw new Error(`No existe una bicicleta con el id sumistrado ${biciId}`);
}

Bicicleta.remove = function(biciId){
    for (let i=0; i<Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == biciId){
            //console.log('id:'+Bicicleta.allBicis[i].id+" - index:"+i);
            Bicicleta.allBicis.splice(i, 1);
            break;
        }            
    }
}

Bicicleta.update = function(updatedBici){
    for (let i=0; i<Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == updatedBici.id){
            //console.log('id:'+Bicicleta.allBicis[i].id+" - index:"+i);
            Bicicleta.allBicis.splice(i, 1, updatedBici);
            break;
        }            
    } 
}

// var bici1 = new Bicicleta(1, 'roja', 'urbana', [0.831018, -77.648061]);
// var bici2 = new Bicicleta(2, 'blanca', 'urbana', [0.830632, -77.644596]);

// Bicicleta.add(bici1);
// Bicicleta.add(bici2);

module.exports = Bicicleta;
*/