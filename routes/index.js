var express = require('express');
var router = express.Router();
let data = {
  title: 'RED BICICLETAS',
  msg:'La primera comunidad online de ciclistas urbanos'
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', data);
});

module.exports = router;
