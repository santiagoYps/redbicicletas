const express = require('express');
let router = express.Router();
let usuarioConstrollerApi = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioConstrollerApi.usuarios_list);
router.post('/create', usuarioConstrollerApi.usuarios_create);
router.post('/reservar', usuarioConstrollerApi.usuarios_reservar);
router.delete('/delete', usuarioConstrollerApi.usuarios_delete);

module.exports = router;