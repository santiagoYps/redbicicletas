const express = require('express');
let router = express.Router();
const reservaControllerAPI = require('../../controllers/api/reservaConstrollerAPI');

router.get('/', reservaControllerAPI.reservas_list);
router.delete('/delete', reservaControllerAPI.reservas_delete);

module.exports = router;