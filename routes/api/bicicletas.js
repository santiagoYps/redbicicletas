let express = require('express');
let router = express.Router();
let bicicletasControllerAPI = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletasControllerAPI.bicicleta_list);
router.post('/create', bicicletasControllerAPI.bicicleta_create);
router.delete('/delete', bicicletasControllerAPI.bicicleta_delete);
router.put('/update', bicicletasControllerAPI.bicicleta_update);

module.exports = router;