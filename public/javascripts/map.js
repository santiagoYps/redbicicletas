var map = L.map('main_map').setView([0.8261788,-77.6725451], 13);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var marker = L.marker([0.823684, -77.635427]).addTo(map);
var marker = L.marker([0.824794, -77.638875]).addTo(map);
var marker = L.marker([0.830152, -77.634459]).addTo(map);

//      Concumientod API
$.ajax({
    dataType: 'json',
    url: '/api/bicicletas',
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
});