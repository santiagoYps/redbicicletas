# Curso: Desarrollo del lado del servidor
#### **Semana 3:** Autenticación

---

**Nombre del Proyecto:** Red de bicicletas. 

**Descripción:** En el siguiente repositorio se encuentran desarrollados todas las lecciones de la semana 1 del curso Desarrollo del lado servidor: NodeJS, Express y MongoDB.

---

**Instrucciones:**

- Abrir la terminal o ventana de comandos del sistema.
- Clonar Repositorio con el comando:
  `git clone https://santiagoYps@bitbucket.org/santiagoYps/redbicicletas.git`
- Instalar dependencias con el comando:
  `npm install`
- Correr el servidor con el comando:
  `npm run devstart`
- Abrir la direccion http://localhost:3000/
- En la página principal se podrá apreciar el mapa con algunas bicicletas precargadas.
- Para entrar a la lista de bicicletas en formato tabla, hacer clic en la opción **Lista de bicicletas** de la barra de navegacion del template editado.
- En esta sección (http://localhost:3000/bicicletas) se pueden verificar las funcionalidades CRUD: Crear, Listar, Actualizar y Eliminar para las bicicletas.

---

- Para verificar el funcionamiento de la API de forma rapida, se sugiere importar las peticiones HTTP utilizadas en Postman desde el siguiente link y siguiento los siguientes pasos:
  `https://www.getpostman.com/collections/37c044302fee193853b5`
1. Abrir postman
2. En la parte superior izquierda buscar el boton **Import**
3. Seleccionar la pestaña **link**
4. Pegar el anterior link
5. Clic en **Continue**

- Como resultado se tendrán las peticiones Get, Post, Delete y Put implementadas para la API.

---

